from ctypes import CDLL, POINTER, c_void_p, c_char_p, c_double

import numpy as np
import pathlib
import platform
import os


class IDEA:
    """
    Python wrapper for IDEA library

    Parameters
    ----------
    model : str
        Trained model (15000K | 25000K)
    err_level : str
        Error Levels
    verbose : bool
        Print details or not
    """
    def __init__(self, model="15000K", err_level="ALL=1, PTD=3, DEP=4", verbose=True):
        # Get library direcory
        wdir = pathlib.Path(__file__).parent

        # get the right library name
        platform_type = platform.uname()[0]
        if platform_type == 'Windows':
            libname = 'libidea.dll'
        elif platform_type == 'Linux':
            libname = 'libidea.so'
        elif platform_type == 'Darwin':
            libname = 'libidea.dylib'
        else:
            raise ValueError('Unsupported OS')
        
        # Load dynamic library
        self._lib = lib = CDLL(os.path.join(wdir, libname))       
        
        # Save verbose
        self._verbose = verbose
        
        # Prototypes
        lib.IDEA_Units.restype=c_char_p
        
        lib.IDEA_Init.argtypes = [c_char_p, c_char_p]
        lib.IDEA_Init.restype = c_char_p
        
        lib.IDEA_Finalize.restype = c_char_p

        # Add method to compute value
        for name in ['DEP', 'DET', 'DEH', 'DES', 'DEA', 'DEC', 'DEG', 'DEV', 'DEK',
                    'PTD', 'PTE', 'PTH', 'PTS', 'PTA', 'PTC', 'PTG', 'PTV', 'PTK',
                    'DPT', 'DPE']:

            # Register arg, res                    
            func = getattr(lib, 'IDEA_{}'.format(name))      
            func.argtypes = [c_double, c_double]
            func.restype = c_double

            # Add method
            setattr(self, name, self._gen_func(func))

        # Add method to compute gradient
        for name in ['DEP', 'DET', 'DEH', 'DES', 'DEA', 'DEC', 'DEG', 'DEV', 'DEK',
                    'PTD', 'PTE', 'PTH', 'PTS', 'PTA', 'PTC', 'PTG', 'PTV', 'PTK',
                    'DPT', 'DPE']:

            # Register arg, res
            func = getattr(lib, 'IDEA_{}_Grad'.format(name))      
            func.argtypes = [c_void_p, c_double, c_double]
            func.restype = c_double

            # Add method
            setattr(self, name+'_Grad', self._gen_grad_func(func))
        
        # Add model folder
        modeldir = os.path.join(wdir, 'IDEA', 'models', model)
        if not modeldir.endswith('/'):
            modeldir +='/'

        # Initialize library
        txt = lib.IDEA_Init(bytes(modeldir, 'utf-8'), bytes(err_level, 'utf-8'))
        if verbose:
            print(txt.decode())

    def Units(self):
        # Support units
        print(self._lib.IDEA_Units().decode())

    def _gen_func(self, func):
        # Method to return value
        def method(*args):
            return func(*args)

        return method

    def _gen_grad_func(self, func):
        # Method to return value and gradient
        def method(*args, grad='none'):
            if grad == 'none':
                # Make array for gradient
                grad = np.empty(2)            
            
            return func(grad.ctypes, *args), grad

        return method        
            
    def __del__(self):
        # Finalize library
        txt = self._lib.IDEA_Finalize()

        if self._verbose:
            print(txt.decode())
