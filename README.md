pyIDEA
======

Overview
--------
Python Wrapper for [IDEA library](https://github.com/HojunYouKr/IDEA)

How to build?
--------------
1. Clone IDEA library

```console
git clone https://github.com/HojunYouKr/IDEA
```

2. Build and Setup

```console
python setup.py build
python setup.py install
```

Example
-------
```python
from pyIDEA import IDEA

idea = IDEA(model='15000K')
rho, e = 0.1, 1.4e5
p = idea.DEP(rho, e)
print("Pressure : {:.3f} Pa".format(p))
```