from setuptools import setup, Extension, find_packages

#  https://stackoverflow.com/questions/4529555/building-a-ctypes-based-c-library-with-distutils
from distutils.command.build_ext import build_ext
import pathlib


class CTypesExtension(Extension):
    pass


class CType_build_ext(build_ext):
    def build_extension(self, ext):
        self._ctypes = isinstance(ext, CTypesExtension)
        return super().build_extension(ext)

    def get_export_symbols(self, ext):
        if self._ctypes:
            return ext.export_symbols
        return super().get_export_symbols(ext)

    def get_ext_filename(self, ext_name):
        if self._ctypes:
            return ext_name + ".so"
        return super().get_ext_filename(ext_name)


# Compile ctype library
libidea = CTypesExtension('libidea', 
    ['IDEA/src/model_basic.cpp', 'IDEA/src/model.cpp'],
    include_dirs=['IDEA/include'],
    libraries=['stdc++']
)

# Additional data
data_packages = [
    'IDEA.models.15000K', 'IDEA.models.25000K'
]

# Hard dependencies
install_requires = [
    'numpy >= 1.10',
]

setup(name='pyIDEA',
    version = '1.0',
    description='Python wrapper for IDEA library',
    author='Jin Seok Park',
    ext_modules = [libidea],
    py_modules=['pyIDEA'],
    packages=find_packages() + data_packages,
    include_package_data=True,
    install_requires=install_requires,
    cmdclass={"build_ext": CType_build_ext},
    )
